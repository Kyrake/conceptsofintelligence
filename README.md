# ConceptsOfIntelligence


LogOutput: [PDF with terminal output of all used functions](https://pages.github.com/)

## Parametric Test
(Average p-values over all Items)

<b>Shapiro Test:</b> 6.912e-13

<b>Levene Test:</b> 2.2e-16

<b>ChiSquare Test: </b>
- Age: 2.2e-16
- Gender: 2.2e-16
- Education: 2.2e-16

<b>Polinomial Regression (gml):</b>

- Age: 9.954515e-01
- Gender: 0.94893e-01
- Education: 0.93893e-01

<b>T-test:</b>
- Age: 2.2e-16
- Gender: 2.2e-16
- Education: 2.2e-16

<b>Cronbach - Alpha:</b> 0.95



## Non-parametric Tests
(Average p-values over all Items)

<b>Kruskal:</b> 
- Age: 0.1360294
- Gender: 0.144028
- Education: 0.1976751

<b>Ordinal Logistic Regression (polr):</b> 0.3233302


# Plots - with Median

![alt text](<./Plots/linpng.png>) 
![alt text](<./Plots/DimensionMedian.png>) 
![alt text](<./Plots/Agepng.png>) 
![alt text](<./Plots/gendermedian.png>)
![alt text](<./Plots/educ.png>) 

# Plots - with Mean

![alt text](<./Plots/Strategies.png>) 
![alt text](<./Plots/Dimension.png>) 
![alt text](<./Plots/Age.png>) 
![alt text](<./Plots/Gender.png>) 
![alt text](<./Plots/Education.png>)

# Plots - Likert & QQ

![alt text](<./Plots/LikertGender.png>) 
![alt text](<./Plots/qqplot.png>) 
 

